# The LOG_LEVEL variable defines how verbose the logging utility should be. I can be one of the following:
# - error
# - warn
# - info (default)
# - verbose
# - debug
# - silly
LOG_LEVEL=info

# This is the port that the service will listen to and it defaults to 3000.  Remember that if this is running 
# inside a Docker container, that port is relative to the container and will need to be opened to the outside 
# so that messages can be sent to it.
SERVER_PORT=3000

# This is 'true' or 'false' and determines if the service should try to forward the notifications to Slack. 
# Defaults to 'false'.
PUBLISH_TO_SLACK=false

# This is the client token needed to push notifications to Slack if it's been enabled. Defaults to empty but 
# is required if Slack notifications are turned on. See README.md for details
SLACK_TOKEN=

# This is the Slack ID of the user that will be a default user to add to a channel.  See README.md for details 
# on how to obtain a Slack user ID. This is required due to the fact that if a channel is to be created and 
# does not exist in Slack yet, it must have a user associated with that channel before it can be created.
SLACK_USER_ID_TO_ADD=

# This is an integer that defines the number of seconds between each message that is pushed to Slack.  Slack 
# has a limit on the frequency of incoming messages that I believe is one message per second.  The default 
# for this is 2 seconds.
SLACK_INTERVAL_IN_SECONDS=2

# This is 'true' or 'false' and tells the server if it should try to foward incoming messages to an AMQP 
# server.  The default is false.
PUBLISH_TO_AMQP=false

# This is the name of the host that AMQP messages will be sent to.  It is required if you are sending messages 
# to an AMQP server and realize it may be relative to the Docker container if running this inside Docker.
AMQP_HOST=

# This is the port that the AMQP server will be listening to.  It is required if you are enabling sending 
# messages to AMQP.
AMQP_PORT=

# This is the VHost that will be published to if AMQP publishing is enabled.  It is required.
AMQP_VHOST=

# The username that will be used to connect to the AMQP server.
AMQP_USERNAME=

# The password that will be used to connect to the AMQP server.
AMQP_PASSWORD=
