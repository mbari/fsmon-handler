# This is a Docker build file that starts with a base Node JS image and then adds the server code to enable
# a fsmon-handler instance.

# Start with the NodeJS image
FROM node:10.15.3

# Expose the port the server will be listening to
EXPOSE ${SERVER_PORT}

# Add the files needed for the application to run
ADD --chown=node server.js /home/node/app/server.js
ADD --chown=node package.json /home/node/app/package.json
ADD --chown=node package-lock.json /home/node/app/package-lock.json

# Switch to the node user
USER node

# Set the working directory
WORKDIR /home/node/app

# Run the package install
RUN npm install

# Now run the app
CMD npm start
