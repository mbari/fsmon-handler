// First, look for an environment variable that defines the level of logging
let LOG_LEVEL = process.env.LOG_LEVEL;

// If one is not defined, declare a default of info
if (!LOG_LEVEL) LOG_LEVEL = 'info';

// Import the necessary object from winston for logging
const {createLogger, format, transports} = require('winston');
const {combine, timestamp, printf} = format;

// Create a custom format for the log file
const myFormat = printf(({level, message, timestamp_to_format}) => {
    return `${timestamp_to_format} [${level}]: ${message}`;
});

// Now create the logger
const logger = createLogger({
    level: LOG_LEVEL,
    format: combine(
        timestamp(),
        myFormat
    ),
    transports: [new transports.Console()]
});

// Log a message that we are starting up
logger.info('fsmon-handler starting up');

// Check the environment for the port the server will listen to
let SERVER_PORT = Number(process.env.SERVER_PORT);

// If one was not defined, set the default port of 3000
if (!SERVER_PORT) SERVER_PORT = 3000;

// Check for Slack related environment variables
const PUBLISH_TO_SLACK = (process.env.PUBLISH_TO_SLACK == 'true' || process.env.PUBLISH_TO_SLACK == 'TRUE');
const SLACK_TOKEN = process.env.SLACK_TOKEN;
const SLACK_USER_ID_TO_ADD = process.env.SLACK_USER_ID_TO_ADD;

// This is the number of seconds to wait between messages being sent to Slack
const SLACK_INTERVAL_IN_SECONDS = 2;

// And lastly, check environment for AMQP variables
const PUBLISH_TO_AMQP = (process.env.PUBLISH_TO_AMQP == 'true' || process.env.PUBLISH_TO_AMQP == 'TRUE');
const AMQP_HOST = process.env.AMQP_HOST;
const AMQP_PORT = process.env.AMQP_PORT;
const AMQP_VHOST = process.env.AMQP_VHOST;
const AMQP_USERNAME = process.env.AMQP_USERNAME;
const AMQP_PASSWORD = process.env.AMQP_PASSWORD;

logger.info('Startup variables:');
logger.info('LOG_LEVEL: ' + LOG_LEVEL);
logger.info('SERVER_PORT: ' + SERVER_PORT);
logger.info('PUBLISH_TO_SLACK: ' + PUBLISH_TO_SLACK);
logger.info('PUBLISH_TO_AMQP: ' + PUBLISH_TO_AMQP);
logger.info('AMQP_HOST: ' + AMQP_HOST);
logger.info('AMQP_PORT: ' + AMQP_PORT);
logger.info('AMQP_VHOST: ' + AMQP_VHOST);
logger.info('AMQP_USERNAME: ' + AMQP_USERNAME);

// Let's do some validation of variables after checking for them

// Check that if Slack publishing is on, make sure we have token and user ID
if (PUBLISH_TO_SLACK && !SLACK_TOKEN) {
    logger.error('You have turned on Slack publishing, but no Slack token was provided, will exit');
    process.exit(-1);
}
if (PUBLISH_TO_SLACK && !SLACK_USER_ID_TO_ADD) {
    logger.error('You have turned on Slack publishing, but no default channel user ID was provided, will exit');
    process.exit(-1);
}

// If AMQP publishing is on, check all the other necessary variables
if (PUBLISH_TO_AMQP && !AMQP_HOST) {
    logger.error('You have turned on AMQP publishing, but no AMQP host was specified, will exit.');
    process.exit(-1);
}
if (PUBLISH_TO_AMQP && !AMQP_PORT) {
    logger.error('You have turned on AMQP publishing, but no AMQP port was specified, will exit.');
    process.exit(-1);
}
if (PUBLISH_TO_AMQP && !AMQP_VHOST) {
    logger.error('You have turned on AMQP publishing, but no AMQP vhost was specified, will exit.');
    process.exit(-1);
}
if (PUBLISH_TO_AMQP && !AMQP_USERNAME) {
    logger.error('You have turned on AMQP publishing, but no AMQP username was specified, will exit.');
    process.exit(-1);
}
if (PUBLISH_TO_AMQP && !AMQP_PASSWORD) {
    logger.error('You have turned on AMQP publishing, but no AMQP password was specified, will exit.');
    process.exit(-1);
}

// Import the slack client library
const { WebClient } = require('@slack/web-api');

// If publishing to slack, create a Slack web API client using the token
let web = null;
if (PUBLISH_TO_SLACK) {
    web = new WebClient(SLACK_TOKEN);
}

// Create an array that can be used to queue up messages for Slack.  Slack will only accept messages at a specific
// rate so we queue them to make sure they come across at an acceptable rate.
const slackMessageQueue = [];

// This function checks a local array of messages to see if any need to be published.  If they do, the function will try
// to send a message and if it fails with no channel found, it will create the channel and resend the message
let sendSlackMessageFromQueue = function () {

    // Look to see if there are any messages to be sent
    if (slackMessageQueue.length > 0) {

        // Grab the message from the top of the queue
        let messageJSON = slackMessageQueue.shift();

        // Use the name of the repo to construct the name of the channel
        let channelName = 'fsmon-' + messageJSON.repoName;

        // If it's longer than 22 characters, truncate it
        if (channelName.length > 22) {
            channelName = channelName.substring(0, 21);
        }
        logger.debug('Slack channel will be ' + channelName);

        // Now let's build the message body for Slack
        let message = '';

        // Find the resource type
        if (messageJSON.resourceType && messageJSON.resourceType == 'FILE') {
            message = message + ':page_facing_up: File was ';
        }
        if (messageJSON.resourceType && messageJSON.resourceType == 'DIRECTORY') {
            message = message + ':file_folder: Directory was ';
        }
        if (!messageJSON.resourceType ||
            (messageJSON.resourceType != 'FILE' && messageJSON.resourceType != 'DIRECTORY')) {
            message = message + ':grey_question: Unknown was  ';
        }

        // Now add on the event
        message += messageJSON.event;

        // Add the date
        if (messageJSON.timestamp) {
            message += ' at ' + '<!date^' + messageJSON.timestamp + '^{date_short} {time_secs}| an unknown time>';
        }

        // And a resourceURL if one exists
        if (messageJSON.resourceURL) {
            message += '\n' + messageJSON.resourceURL;
        }
        logger.debug('Sending message: ');
        logger.debug(message);

        // Let's just try to post a message
        web.chat.postMessage({
            channel: channelName,
            text: message
        }, function (err, res) {
            // Check for an error
            if (err) {
                // Check to see if it's that a channel was not found
                if (err.data.error == 'channel_not_found') {
                    logger.debug('Channel was not found, will try to create one');

                    // Since the channel was not found, let's create it.
                    web.conversations.create({
                        token: SLACK_TOKEN,
                        name: channelName,
                        is_private: false,
                        user_ids: SLACK_USER_ID_TO_ADD
                    }, function (createErr, createRes) {
                        // Check for any errors trying to create the channel
                        if (createErr) {
                            logger.error('Error trying to create channel');
                            logger.error(createErr);
                        } else {
                            // Log the success response for debugging
                            logger.debug('Channel created: ');
                            logger.debug(createRes);

                            // Now, let's set the channel purpose
                            // TODO kgomes - this does not work, I need to get the channel ID from the createRes and use that
                            web.conversations.setPurpose({
                                token: SLACK_TOKEN,
                                channel: createRes.channel.id,
                                purpose: 'Any file system changes in repository ' + messageJSON.repoName +
                                    ' are published to this channel'
                            }, function (spErr, spRes) {
                                // Check for errors
                                if (spErr) {
                                    logger.error('Error trying to set purpose on channel');
                                    logger.error(spErr);
                                } else {
                                    // Log success for debugging
                                    logger.debug('Channel purpose set');
                                    logger.debug(spRes);

                                    // And finally, post the message
                                    web.chat.postMessage({
                                        channel: channelName,
                                        text: message
                                    }, function (err2, res2) {
                                        // Look for an error
                                        if (err2) {
                                            logger.error('Error caught trying to post a message after creating channel ' +
                                                channelName);
                                            logger.error(err2);
                                        } else {
                                            logger.debug('Message sent successfully to slack');
                                            logger.debug(res2);
                                        }
                                    });
                                }
                            });
                        }
                    });
                } else {
                    logger.error('Error trying to post message to slack channel ' + channelName);
                    logger.error(err);
                }
            } else {
                // Just log for debugging
                logger.debug(res);
            }
        });
    }
};

// It the user requested message being sent to slack, fire up an interval to check the queue and send any messages
if (PUBLISH_TO_SLACK) {
    setInterval(sendSlackMessageFromQueue, SLACK_INTERVAL_IN_SECONDS * 1000);
}

// Import the amqp library
const amqp = require('amqplib/callback_api');

// An AMQP channel that will be needed for publishing of messages (if enabled)
let amqpChannel;

// Define a function to connect to the AMQP server
let connectToAMQP = function () {
    // Create the URL using the environment variables
    let amqpURL = 'amqp://';

    // if there is a username and password, add them to the URL
    if (AMQP_USERNAME && AMQP_PASSWORD) {
        amqpURL += AMQP_USERNAME + ':' + AMQP_PASSWORD + '@';
    }

    // Add the host
    amqpURL += AMQP_HOST;

    // if the port was specified add that
    if (AMQP_PORT) amqpURL += ':' + AMQP_PORT;

    // And add VHOST if specified
    if (AMQP_VHOST) amqpURL += '/' + AMQP_VHOST;
    logger.debug('Will try to connect to AMQP server at ' + AMQP_HOST + ':' + AMQP_PORT);

    // Let's connect to AMQP
    amqp.connect(amqpURL, function (err, connection) {

        // Check for an error and if one exists, we should exit this process as we did not connect to the AMQP server
        if (err) {
            logger.error('Error connecting to AMQP server, will try again in 5 seconds.');
            logger.error(err);
            // Try again after a bit
            setTimeout(connectToAMQP,5000);
        } else {
            logger.debug('Connected to AMQP server');

            // Since the connection was successful, create a channel
            connection.createChannel(function (error, channel) {
                // Check for an error and if one occurred, exit since we need the connection and channel
                if (error) {
                    logger.error('Could not create channel on AMQP server, existing ...');
                    logger.error(error);
                } else {
                    logger.debug('Successfully created channel on AMQP server');
                    amqpChannel = channel;
                }
            });
        }
    });
};

// Check to see if AMQP publishing is enabled
if (PUBLISH_TO_AMQP) {
    // Connect to the AMQP server
    connectToAMQP();
}

// Grab the express extension to winston logging
var expressWinston = require('express-winston');

// Import the body parser module
const bodyParser = require('body-parser');

// Now grab the express module so we can spin up an HTTP server
const express = require('express');

// Create the express application server
const app = express();

// Add the winston express logger
app.use(expressWinston.logger({
    winstonInstance: logger
}));

// Add the body parser
app.use(bodyParser.json());

// Add a route to handle posts requests
app.post('/', (req, res) => {
    logger.debug('Got a post request');
    if (req.body) {
        if (req.body.timestamp) {
            logger.debug('Timestamp: ' + req.body.timestamp);
        }
        if (req.body.event) {
            logger.debug('Event: ' + req.body.event);
        }
        if (req.body.repoName) {
            logger.debug('RepoName: ' + req.body.repoName);
        }
        if (req.body.resourceURL) {
            logger.debug('ResourceURL: ' + req.body.resourceURL);
        }
        if (req.body.resourceType) {
            logger.debug('ResourceType: ' + req.body.resourceType);
        }
    }

    // Make sure we have a repository name as that is a key used for Slack and AMQP
    if (req.body.repoName) {

        // Check to see if AMQP publishing is on
        if (PUBLISH_TO_AMQP) {

            // Establish the exchange using the repo name
            amqpChannel.assertExchange(req.body.repoName, 'fanout', {durable: false}, function (err, ok) {
                // Check for an error
                if (err) {
                    logger.error('Error trying to assert an Exchange named ' + req.body.repoName);
                    logger.error(err);
                } else {
                    // Forward the message
                    amqpChannel.publish(req.body.repoName, '', Buffer.from(JSON.stringify(req.body)));
                    logger.debug('Message sent to AMQP server');
                }
            });
        }

        // If Slack publishing is on, add the message to the queue to be sent
        if (PUBLISH_TO_SLACK) slackMessageQueue.push(req.body);
    } else {
        logger.warn('Message received but no repository name so cannot do anything with it');
        logger.warn(req.body);
    }

    // Forward this to an AMQP exchange
    // TODO kgomes make this more useful
    res.send('Got your message');
});

// Add the error logger
app.use(expressWinston.errorLogger({
    winstonInstance: logger
}));

// Now start server and listen on a port
app.listen(SERVER_PORT, () => logger.info(`fsmon-handler listening on port ${SERVER_PORT}!`));
