# File System Monitor Handler (fsmon-handler)

This project contains code that creates an HTTP server that is designed to accept and handle messages that come from the [fsmon](https://bitbucket.org/mbari/fsmon/src/master/) service.  The idea is that the fsmon service watches for changes in a file system and pushes those event to something like this fsmon-handler.  The fsmon-handler examines the message, and based on the way it is configured, will structure that message in JSON format and publish to AMQP and/or push the message to Slack.  Since it is an HTTP server, it can be the handler for several fsmon instances.

Please consult the documentation in the 'mkdocs' directory.

If you want to see and/or develop the documentation for this project:

1. Install mkdocs
1. Install mkdocs-material
1. Check out this repo
1. cd into the mkdocs dir
1. Run ```mkdocs serve```
1. Point your browser to [http://localhost:8000](http://localhost:8000) to see the documentation. If you edit any files under the mkdocs directory, they will update in your browser after saving.
