#!/bin/sh

# Create Rabbitmq user
( sleep 20 ; \
rabbitmqctl add_user testadmin testadminpass 2>/dev/null ; \
rabbitmqctl set_user_tags testadmin administrator management ; \
rabbitmqctl set_permissions -p / testadmin  ".*" ".*" ".*" ; \
rabbitmqctl add_vhost testvhost ; \
rabbitmqctl add_user testclientuser testclientpass 2>/dev/null ; \
rabbitmqctl set_permissions -p testvhost testadmin  ".*" ".*" ".*" ; \
rabbitmqctl set_permissions -p testvhost testclientuser  ".*" ".*" ".*" ; \
echo "*** User testclientuser completed. ***") & \

# $@ is used to pass arguments to the rabbitmq-server command.
# For example if you use it like this: docker run -d rabbitmq arg1 arg2,
# it will be as you run in the container rabbitmq-server arg1 arg2
rabbitmq-server $@
