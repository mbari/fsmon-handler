// This is a test script that subscribes to a fanout exchange on a rabbitmq server, sends a message to a URL that
// should be an instance of the fsmon-handler server and then makes sure it gets a response and it matches what was sent

// Import the library to manipulate file paths
const path = require('path');

// First thing is to set up the application logging.  First let's see if there is an environment variable
// that specifies the log directory
let LOG_DIRECTORY = process.env.LOG_DIRECTORY;

// If it was not defined, let's just use a 'logs' directory under the current working directory
if (!LOG_DIRECTORY) LOG_DIRECTORY = path.join(__dirname, 'logs');

// Now that we have the log directory, create the full path to the log file
const serverLogPath = path.join(LOG_DIRECTORY, 'sub-pub-test.log');

// // Now look for an environment variable that defines the level of logging
let LOG_LEVEL = process.env.LOG_LEVEL;

// If one is not defined, declare a default of info
if (!LOG_LEVEL) LOG_LEVEL = 'info';

const { createLogger, format, transports } = require('winston');
const { combine, timestamp, printf } = format;

// Create a custom format for the log file
const myFormat = printf(({ level, message, timestamp }) => {
    return `${timestamp} [${level}]: ${message}`;
});

// Now create the logger
const logger = createLogger({
    level: LOG_LEVEL,
    format: combine(
        timestamp(),
        myFormat
    ),
    transports: [new (transports.File)({ filename: serverLogPath })]
});

// Grab testing environment variables
let FSMON_HANDLER_HOST = process.env.FSMON_HANDLER_HOST;
if (!FSMON_HANDLER_HOST) FSMON_HANDLER_HOST = 'localhost';

let FSMON_HANDLER_PORT = Number(process.env.FSMON_HANDLER_PORT);
if (!FSMON_HANDLER_PORT) FSMON_HANDLER_PORT = 3000;

logger.info("Messages will be sent to fsmon-handler listening on host " + FSMON_HANDLER_HOST + " at port " +
    FSMON_HANDLER_PORT);

let TEST_REPO_NAME = process.env.TEST_REPO_NAME;
if (!TEST_REPO_NAME) TEST_REPO_NAME = 'test-sub-pub';

logger.info("Repo that will be used is " + TEST_REPO_NAME);

let TEST_RESOURCE_URL = process.env.TEST_RESOURCE_URL;
if (!TEST_RESOURCE_URL) TEST_RESOURCE_URL =
    'https://media.licdn.com/dms/image/C560BAQFVtPkQxushsw/company-logo_200_200/0?e=2159024400&v=beta&t=VdP6iBwhDRs1NGUiSJQIliX-J1PGqkjsSuXUFQTwOTg';

const AMQP_HOST = process.env.AMQP_HOST || 'localhost';
const AMQP_PORT = process.env.AMQP_PORT || '5672';
const AMQP_VHOST = process.env.AMQP_VHOST || 'fsmon';
const AMQP_USERNAME = process.env.AMQP_USERNAME || 'guest';
const AMQP_PASSWORD = process.env.AMQP_PASSWORD || 'guest';

// Import the http library
const http = require('http');

// Local server options
const options = {
    hostname: FSMON_HANDLER_HOST,
    port: FSMON_HANDLER_PORT,
    path: '',
    method: 'POST',
    headers: {
        'Content-Type': 'application/json'
    }
};

// A function to send a message to the local fsmon-handler
const sendPOST = function (epochSeconds) {

    // The HTTP request
    const req = http.request(options, function (res) {
        res.setEncoding('utf8');
        res.on('data', function (body) {
            logger.debug('Response body: ' + body);
        });
    });

    // Log any errors
    req.on('error', function (e) {
        logger.error('There was a problem with the request: ' + e.message);
    });

    // The message in JSON object
    const form_data = {
        'timestamp': epochSeconds,
        'event': 'Updated',
        'repoName': TEST_REPO_NAME,
        'resourceURL': TEST_RESOURCE_URL,
        'resourceType': 'FILE',
        'size': 12288,
        'modTimestamp': epochSeconds
    };

    // Convert to a string for publishing
    const form_string = JSON.stringify(form_data);
    logger.info("Sending new message");
    logger.info(form_string);

    // Send it
    req.write(form_string);

    // End it
    req.end();
};

// Import the amqp library
const amqp = require('amqplib/callback_api');

// Create the URL using the environment variables
let amqpURL = 'amqp://';

// if there is a username and password, add them to the URL
if (AMQP_USERNAME && AMQP_PASSWORD) {
    amqpURL += AMQP_USERNAME + ':' + AMQP_PASSWORD + '@';
}

// Add the host
amqpURL += AMQP_HOST;

// if the port was specified add that
if (AMQP_PORT) amqpURL += ':' + AMQP_PORT;

// And add VHOST if specified
if (AMQP_VHOST) amqpURL += '/' + AMQP_VHOST;

logger.info("AMQP URL: " + amqpURL);

// First let's connect to the AMQP server and subscribe to a queue that is bound to the proper exchange
amqp.connect(amqpURL, function (err, connection) {

    // Check for error
    if (err) {
        console.log("Error connecting to AMQP server, exiting ... : ");
        console.log(err);
        logger.error("Error connecting to AMQP server, exiting ... : ");
        logger.error(err);
        process.exit(-1);
    } else {
        logger.info("Connected to AMQP server");

        // Set up a way to close the connection after the process is killed
        process.once('SIGINT', function () {
            logger.info("Closing AMQP connection...");
            connection.close();
        });

        // Now create the proper channel
        connection.createChannel(function (err, channel) {
            // Check for an error
            if (err) {
                console.log("Error trying to create a channel, exiting ... : ");
                console.log(err);
                logger.error("Error trying to create a channel, exiting ... : ");
                logger.error(err);
                process.exit(-1);
            } else {
                // Make sure the exchange exists
                channel.assertExchange(TEST_REPO_NAME, 'fanout', { durable: false }, function (err, ok) {

                    // Check for an error
                    if (err) {
                        console.log("Error trying to assert exchange, exiting ... :");
                        console.log(err);
                        logger.error("Error trying to assert exchange, exiting ... :");
                        logger.error(err);
                        process.exit(-1);
                    } else {
                        // Now the exchange should be there, so let's create a queue
                        channel.assertQueue('', { exclusive: true }, function (queueErr, queueOK) {
                            // Check for an error
                            if (queueErr) {
                                console.log("Error asserting a queue, exiting ... :");
                                console.log(queueErr);
                                logger.error("Error asserting a queue, exiting ... :");
                                logger.error(queueErr);
                                process.exit(-1);
                            } else {

                                // Bind the queue to the exchange so messages to the exhange will flow into the queue
                                channel.bindQueue(ok.queue, TEST_REPO_NAME, '', {}, function (bindErr, bindOK) {
                                    // Check for error
                                    if (bindErr) {
                                        console.log("Error binding queue to exchange, exiting ... :");
                                        console.log(bindErr);
                                        logger.error("Error binding queue to exchange, exiting ... :");
                                        logger.error(bindErr);
                                        process.exit(-1);
                                    } else {
                                        // Define the function to run when a message arrives
                                        function handleMessage (message) {
                                            logger.info("Received message from queue!");
                                            let messageContent = message.content;
                                            let messageString = messageContent.toString();
                                            logger.info(messageString);
                                        }

                                        // Consume messages using function
                                        channel.consume(ok.queue, handleMessage, { noAck: true }, function (consumeErr, consumeOK) {
                                            // Check for an error
                                            if (consumeErr) {
                                                console.log("Error trying to set up consumer, exiting ... :");
                                                console.log(consumeErr);
                                                logger.error("Error trying to set up consumer, exiting ... :");
                                                logger.error(consumeErr);
                                                process.exit(-1);
                                            } else {
                                                logger.info("Consumer set up, will start sending timed messages");
                                                setInterval(function () {
                                                    // Grab the current epoch timestamps
                                                    let epochMilliseconds = (new Date).getTime();
                                                    let epochSeconds = Math.trunc(epochMilliseconds / 1000);
                                                    sendPOST(epochSeconds);
                                                }, 10000);
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });
    }
});

