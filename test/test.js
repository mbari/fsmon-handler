// This is a test script that subscribes to a fanout exchange on a rabbitmq server, then starts making changes
// to files in a test directory that should be monitored by fsmon and looks for resulting messages on the
// RabbitMQ exchange

// Import the library to manipulate file paths
const path = require('path');

// And the library for file manipulation
const fs = require('fs');

// // Now look for an environment variable that defines the level of logging
let LOG_LEVEL = process.env.LOG_LEVEL || 'debug';

const { createLogger, format, transports } = require('winston');
const { combine, timestamp, printf } = format;

// Create a custom format for the log file
const myFormat = printf(({ level, message, timestamp }) => {
    return `${timestamp} [${level}]: ${message}`;
});

// Now create the logger
const logger = createLogger({
    level: LOG_LEVEL,
    format: combine(
        timestamp(),
        myFormat
    ),
    transports: [new transports.Console()]
});

// Create a varaible that can track the last file change made
let lastChange = 'none';

// Grab the path defined in the environment where files will be changed
let HOST_TEST_DIR = process.env.HOST_TEST_DIR || ".";
let testFile = path.join(HOST_TEST_DIR, 'testing-file.txt');
logger.debug("Will use the following file as the test file: " + testFile);

// Define the connection parameters to the local RabbitMQ server
const AMQP_HOST = process.env.AMQP_HOST || 'localhost';
const AMQP_PORT = process.env.AMQP_PORT || '5672';
const AMQP_VHOST = process.env.AMQP_VHOST || 'testvhost';
const AMQP_USERNAME = process.env.AMQP_USERNAME || 'testclientuser';
const AMQP_PASSWORD = process.env.AMQP_PASSWORD || 'testclientpass';

// Import the amqp library
const amqp = require('amqplib/callback_api');

// Create the URL using the environment variables
let amqpURL = 'amqp://';

// if there is a username and password, add them to the URL
if (AMQP_USERNAME && AMQP_PASSWORD) {
    amqpURL += AMQP_USERNAME + ':' + AMQP_PASSWORD + '@';
}

// Add the host
amqpURL += AMQP_HOST;

// if the port was specified add that
if (AMQP_PORT) amqpURL += ':' + AMQP_PORT;

// And add VHOST if specified
if (AMQP_VHOST) amqpURL += '/' + AMQP_VHOST;

logger.info("AMQP URL: " + amqpURL);

// First let's connect to the AMQP server and subscribe to a queue that is bound to the proper exchange
amqp.connect(amqpURL, function (err, connection) {

    // Check for error
    if (err) {
        console.log("Error connecting to AMQP server, exiting ... : ");
        console.log(err);
        logger.error("Error connecting to AMQP server, exiting ... : ");
        logger.error(err);
        process.exit(-1);
    } else {
        logger.info("Connected to AMQP server");

        // Set up a way to close the connection after the process is killed
        process.once('SIGINT', function () {
            logger.info("Closing AMQP connection...");
            connection.close();
        });

        // Now create the proper channel
        connection.createChannel(function (err, channel) {
            // Check for an error
            if (err) {
                console.log("Error trying to create a channel, exiting ... : ");
                console.log(err);
                logger.error("Error trying to create a channel, exiting ... : ");
                logger.error(err);
                process.exit(-1);
            } else {
                // Make sure the exchange exists
                channel.assertExchange('test', 'fanout', { durable: false }, function (err, ok) {

                    // Check for an error
                    if (err) {
                        console.log("Error trying to assert exchange, exiting ... :");
                        console.log(err);
                        logger.error("Error trying to assert exchange, exiting ... :");
                        logger.error(err);
                        process.exit(-1);
                    } else {
                        // Now the exchange should be there, so let's create a queue
                        channel.assertQueue('', { exclusive: true }, function (queueErr, queueOK) {
                            // Check for an error
                            if (queueErr) {
                                console.log("Error asserting a queue, exiting ... :");
                                console.log(queueErr);
                                logger.error("Error asserting a queue, exiting ... :");
                                logger.error(queueErr);
                                process.exit(-1);
                            } else {

                                // Bind the queue to the exchange so messages to the exhange will flow into the queue
                                channel.bindQueue(ok.queue, 'test', '', {}, function (bindErr, bindOK) {
                                    // Check for error
                                    if (bindErr) {
                                        console.log("Error binding queue to exchange, exiting ... :");
                                        console.log(bindErr);
                                        logger.error("Error binding queue to exchange, exiting ... :");
                                        logger.error(bindErr);
                                        process.exit(-1);
                                    } else {
                                        // Define the function to run when a message arrives
                                        function handleMessage (message) {
                                            logger.info("Received message from queue!");
                                            let messageContent = message.content;
                                            let messageString = messageContent.toString();
                                            logger.info(messageString);
                                        }

                                        // Consume messages using function
                                        channel.consume(ok.queue, handleMessage, { noAck: true }, function (consumeErr, consumeOK) {
                                            // Check for an error
                                            if (consumeErr) {
                                                console.log("Error trying to set up consumer, exiting ... :");
                                                console.log(consumeErr);
                                                logger.error("Error trying to set up consumer, exiting ... :");
                                                logger.error(consumeErr);
                                                process.exit(-1);
                                            } else {
                                                logger.info("Consumer set up, will start making file changes");
                                                setInterval(function () {
                                                    // First, let's see if the test file exists already
                                                    if (fs.existsSync(testFile)) {
                                                        logger.debug("The test file exists already");
                                                        // Let's check the last step to see if it was an append or not
                                                        if (lastChange === 'append') {
                                                            logger.debug("The last change made was an append, so let's delete the file");
                                                            lastChange = "delete";
                                                            try {
                                                                fs.unlinkSync(testFile);
                                                            } catch (err) {
                                                                logger.error(err);
                                                            }
                                                        } else {
                                                            logger.debug("The last change was not an append, so let's append to the file");
                                                            lastChange = "append";
                                                            try {
                                                                fs.appendFileSync(testFile, 'append');
                                                            } catch (err) {
                                                                // An error occurred
                                                                logger.error(err);
                                                            }
                                                        }
                                                    } else {
                                                        logger.debug("The test file does not exist yet");
                                                        // Since no file exists, let's create it will some filler
                                                        try {
                                                            fs.writeFileSync(testFile, 'filler');
                                                        } catch (err) {
                                                            // An error occurred
                                                            logger.error(err);
                                                        }
                                                    }

                                                    // Grab the current epoch timestamps
                                                    // let epochMilliseconds = (new Date).getTime();
                                                    // let epochSeconds = Math.trunc(epochMilliseconds / 1000);
                                                    // sendPOST(epochSeconds);
                                                    logger.debug("File changed, last state: " + lastChange);
                                                }, 10000);
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });
    }
});

